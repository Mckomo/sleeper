//
//  AppDelegate.m
//  timer
//
//  Created by Mckomo on 12-08-03.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize textField = _textField;
@synthesize checkBox = _checkBox;
@synthesize button = _button;
@synthesize timer = _timer;

- (void)dealloc
{
    [super dealloc];
}
	
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Creating new instance of @timer class
    Timer *aTimer = [[Timer alloc] initWithTextFiled:([self textField])];
    self.timer =  aTimer;
}

- (IBAction)timerToggle:(id)sender {
    
    // Log
    NSLog(@"Hit on button.");
    
    NSString *actionName = [self.button title];
    
    // Start @timer
    if ( [actionName isEqualToString:(@"Sleep")] ) {

        float sleepTime = [self.textField intValue];
        BOOL systemShutdown = [self.checkBox state];
        
        // Set timer config
        [self.timer setSleepTime:(sleepTime)];
        [self.timer setSystemShutdown:(systemShutdown)];
        
        // Update interface
        [self.button setTitle:(@"Stop")];
        [self.textField setEnabled:(NO)];
        
        // Start timer
        [self.timer start];
        
    } 
    // Stop @timer
    else {
        
        // Update interface
        [self.button setTitle:(@"Sleep")];
        [self.textField setEnabled:(YES)];
        [self.textField setIntValue:([self.timer sleepTime])];
        [self.textField setTextColor:([NSColor blackColor])];
        
        // Stop timer
        [self.timer stop];
        
    }
    
	// 
    
}

@end
