//
//  AppDelegate.h
//  Sleeper
//
//  Created by Mckomo on 12-08-03.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "Timer.h"

@class Timer;
@interface AppDelegate : NSObject <NSApplicationDelegate> 


@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *textField;
@property (assign) IBOutlet NSButton *checkBox;
@property (assign) IBOutlet NSButton *button;
@property (strong) Timer *timer;

- (IBAction)timerToggle:(id)sender;

@end
