//
//  Timer.m
//  Timer
//
//  Created by Mckomo on 12-08-03.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Timer.h"


// Constants 
int const static TIMER_SLEEP_TIME = 60;
BOOL const static TIMER_SHUTDOWN_SYSTEM = NO;
BOOL const static TIMER_ANIMATE_DOTS_ADDING = YES;


@implementation Timer

@synthesize sleepTime = _sleepTime;
@synthesize systemShutdown = _systemShutdown;

// Constructor
-(Timer*) initWithTextFiled:(NSTextField *)textFieldReference {
    
    self = [super init];
    
    // Set text filed
    textField = textFieldReference;
    
    // Init system class
    
    // Set defaults
    [self setSleepTime:(TIMER_SLEEP_TIME)];
    [self setSystemShutdown:(TIMER_SHUTDOWN_SYSTEM)];
    animateDotsAdding = TIMER_ANIMATE_DOTS_ADDING;
    
    return self;
    
};

-(void) start {
    
    // Validate data
    if ( self.sleepTime <= 0 )
        self.sleepTime = 0;
    
    countSecounds = [self sleepTime]*60-1;
    
	// initialize timer no two to count each 5 seconds
	timer = [NSTimer scheduledTimerWithTimeInterval:1 
                                            target:self 
                                            selector:@selector(updateTimer:) 
                                            userInfo:nil 
                                            repeats:YES];
    
    // Log
    NSLog(@"Timer starts.");
    
};

-(void) stop {
    
    [timer invalidate];    
    timer = nil;
    
    // Log
    NSLog(@"Timer stoped.");
    
};

-(void) finish {
        
    // Log
    NSLog(@"Countdown is finished.");
    
    // Terminate timer interval
    [self stop];
    
    // Terminate one of programs
    if ( [self systemShutdown] == YES ) {
        
        // Log
        NSLog(@"Shutting down system!");
        
        // Shutting down system
        
        NSAppleScript *appleScript = [[[NSAppleScript alloc] initWithSource:(@"tell application \"System Events\" to shut down")] autorelease];
        NSDictionary *errDict = nil;
        
        if (![appleScript executeAndReturnError:&errDict]) {
            NSLog([errDict description]); 
        }
        
    } 
    // Terminate, by default, iTunes 
    else {
        
        // Log
        NSLog(@"Killing iTunes!");
        
        // Killing iTunes
        system([@"killall iTunes" UTF8String]);

    }
    
    // Give a notice to user iterface
    [textField setStringValue:(@"Time is over!")];
    
}

-(void) updateTimer:(NSTimer*) timer {
    
    countSecounds--;
    
    // If countdown is over terminate program
    if ( countSecounds < 0 ) {
        
        [self finish];
        return;
    } 
    
    // Update user inteface
    [self updateInterface];
}


-(void) updateInterface {
    
    int countDots = countSecounds%4;
    NSString* toDisplay;
        
    // Set red font at text field for last 60 secounds
    if ( countSecounds == 60 )
        [textField setTextColor:([NSColor redColor])];
    
    // Set display format. If more then 60 secounds, show minutes and animate dots
    if ( countSecounds > 60) {
        
        toDisplay = [NSString stringWithFormat:(@" %d "), countSecounds/60 ];
        
        // Animate textfield adding dots
        if ( animateDotsAdding == YES) {
            
            for (int i=1;  i <= 3-countDots ;i++) {
                toDisplay = [NSString stringWithFormat:(@" %@."), toDisplay];
            }
            
            if ( countDots == 0 ){
                animateDotsAdding = NO;
            }
            
        // Animate textfield removing dots        
        } else {
            
            for (int i=1;  i <= countSecounds%4 ;i++) {
                toDisplay = [NSString stringWithFormat:(@" %@."), toDisplay];
            }
            
            if ( countDots == 0 ){
                animateDotsAdding = YES;
            }        
        }
    } 
    // Else show secounds without dots
    else {
        toDisplay = [NSString stringWithFormat:(@" .%d "), countSecounds];
    }
    
    [textField setStringValue:(toDisplay)];
};


@end
