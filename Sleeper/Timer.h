//
//  Sleeper.h
//  Sleeper
//
//  Created by Mckomo on 12-08-03.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// Constants 
static int const TIMER_SLEEP_TIME;
static BOOL const TIMER_SHUTDOWN_SYSTEM;
static BOOL const  TIMER_ANIMATE_DOTS_RIGHT;

@interface Timer : NSObject {
    NSTimer* timer;
    int countSecounds;
    BOOL isRuning;
    NSTextField* textField;
    BOOL animateDotsAdding;
}


@property (assign) int sleepTime;
@property (assign) BOOL systemShutdown;

// Constructor
-(Timer*) initWithTextFiled:(NSTextField*) textFieldReference;

// Basic sleeper actions
-(void) start;
-(void) stop;
-(void) finish;

// Logic of sleeper 
-(void) updateTimer:(NSTimer*) timer;


// Update userinterface
-(void) updateInterface;

@end

